import type { NextApiRequest, NextApiResponse } from "next"

let state: "full" | "partial" | "empty" = "empty"

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method === "POST") {
    if (req.query.state === "full") {
      state = "full"
    } else if (req.query.state === "partial") {
      state = "partial"
    } else {
      state = "empty"
    }

    res.status(200).end()
  } else {
    res.status(200).json({ state })
  }
}
