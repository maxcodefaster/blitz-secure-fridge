import type { NextApiRequest, NextApiResponse } from "next"

export type AuthLog = {
  id: string
  timestamp: Date
}

const demoUserId = "1476301241"
const authLog: AuthLog[] = [{ id: demoUserId, timestamp: new Date() }]

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method === "GET" && !req.query?.id) {
    res.status(200).json(authLog)
    return
  }

  authLog.push({
    id: req.query.id as string,
    timestamp: new Date(),
  })

  if (demoUserId === req.query.id) {
    res.status(200).json({ id: demoUserId, firstName: "Jane", lastName: "Doe" })
  } else {
    res.status(404).end()
  }
}
