import { BlitzPage } from "blitz"
import { useEffect, useState } from "react"
import { AuthLog } from "../api/users"

const Home: BlitzPage = () => {
  const [logs, setLogs] = useState<AuthLog[]>([])
  const [spaceStatus, setSpaceStatus] = useState<string | null>(null)

  const apiUrl =
    process.env.NODE_ENV === "development"
      ? "http://localhost:3000/" // development api
      : "https://blitz-secure-fridge.vercel.app/" // production api

  const getAllLogs = async () => {
    const res = await fetch(apiUrl + "api/users")
    return res.json()
  }

  const getSpaceStatus = async () => {
    const res = await fetch(apiUrl + "api/space")
    return res.json()
  }

  useEffect(() => {
    getAllLogs().then((x: AuthLog[]) => setLogs(x))
  }, [])

  useEffect(() => {
    getSpaceStatus().then((x: { state: string }) => setSpaceStatus(x.state))
  }, [])

  const refresh = async () => {
    await Promise.all([
      fetch(apiUrl + "api/users?id=" + Math.random().toString().slice(2, 12)),
      fetch(apiUrl + "api/space?state=partial", { method: "POST" }),
    ])

    getAllLogs().then((x: AuthLog[]) => setLogs(x))
    getSpaceStatus().then((x: { state: string }) => setSpaceStatus(x.state))
  }

  const renderRow = (log: AuthLog) => {
    return (
      <tr key={log.id}>
        <td>
          <b>{log.id}</b>
        </td>
        <td>
          <i>{log.timestamp.toString()}</i>
        </td>
      </tr>
    )
  }

  return (
    <div className="container">
      <h1>smartSecureFridge</h1>
      <p>
        Fill state fridge space 1: <i>{spaceStatus}</i>
      </p>
      <main>
        <table>
          <thead>
            <tr>
              <th>ID</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody>
            {logs && logs.map(renderRow)}
            {logs && !logs.length && (
              <tr>
                <td className="text-center">
                  <div className="p-2">No Logs To Display</div>
                </td>
              </tr>
            )}
          </tbody>
        </table>
      </main>

      <footer>
        <button onClick={refresh}>Refresh</button>
      </footer>

      <style jsx global>{`
        @import url("https://fonts.googleapis.com/css2?family=Libre+Franklin:wght@300;700&display=swap");

        html,
        body {
          padding: 0;
          margin: 0;
          font-family: "Libre Franklin", -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen,
            Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
        }

        * {
          -webkit-font-smoothing: antialiased;
          -moz-osx-font-smoothing: grayscale;
          box-sizing: border-box;
        }

        .container {
          min-height: 100vh;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }

        main {
          padding: 5rem 0;
          flex: 1;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }

        main p {
          font-size: 1.2rem;
        }

        p {
          text-align: center;
        }

        footer {
          width: 100%;
          height: 60px;
          border-top: 1px solid #eaeaea;
          display: flex;
          justify-content: center;
          align-items: center;
          background-color: #45009d;
        }

        footer a {
          display: flex;
          justify-content: center;
          align-items: center;
        }

        footer a {
          color: #f4f4f4;
          text-decoration: none;
        }

        pre {
          background: #fafafa;
          border-radius: 5px;
          padding: 0.75rem;
          text-align: center;
        }

        code {
          font-size: 0.9rem;
          font-family: Menlo, Monaco, Lucida Console, Liberation Mono, DejaVu Sans Mono,
            Bitstream Vera Sans Mono, Courier New, monospace;
        }

        .grid {
          display: flex;
          align-items: center;
          justify-content: center;
          flex-wrap: wrap;

          max-width: 800px;
          margin-top: 3rem;
        }

        @media (max-width: 600px) {
          .grid {
            width: 100%;
            flex-direction: column;
          }
        }
      `}</style>
    </div>
  )
}

Home.suppressFirstRenderFlicker = true

export default Home
